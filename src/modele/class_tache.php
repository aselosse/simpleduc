<?php

class Tache {

    private $db;
    private $insert;
    private $select;
    private $delete;
    private $update;
    private $selectById;
    private $selectByIdProjet;
    private $selectDevById;
    private $sumCoutByProjet;
    private $insertTaskDev;
    private $updateTaskDev;
    private $deleteDevTask;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into TACHES(libelle, projet,statut) values (:libelle, :projet, 1)");
        $this->select = $db->prepare("select p.id, u.nom, chef,description, p.nom as nomprojet, u.prenom, c.nom as nomCli from PROJET p inner join UTILISATEUR u on p.chef = u.id  inner join CLIENT c on p.client=c.id order by p.nom ");
        $this->delete = $db->prepare("delete from PROJET where id=:id");
        $this->update = $db->prepare("update TACHES SET statut=:statut where id=:id");
        $this->selectById = $db->prepare("select id,client, nom, chef,cout,description from PROJET where id=:id order by nom");
        $this->selectByIdProjet = $db->prepare("select id, libelle,cout, statut from TACHES where projet=:projet");
        $this->selectDevById = $db->prepare(" SELECT t.id,t.libelle as libelleTache,t.projet,s.libelle as libelleStatut "
                . " FROM UTILISATEUR u INNER JOIN TACHE_DEV td ON u.id = td.id_utilisateur"
                . " INNER JOIN TACHES t ON td.id_tache = t.id"
                . " INNER JOIN PROJET p ON t.projet = p.id"
                . " INNER JOIN STATUT s ON t.statut = s.id"
                . " WHERE p.id = :projet AND td.id_utilisateur = :idUtilisateur");
        $this->sumCoutByProjet = $db->prepare("SELECT sum(cout) as cout FROM TACHES WHERE projet=:projet ");
        $this->insertTaskDev = $db->prepare("INSERT INTO TACHE_DEV(id_utilisateur, id_projet, id_tache) VALUES(:id_utilisateur, :id_projet, :id_tache)");
        $this->updateTaskDev = $db->prepare("UPDATE TACHE_DEV SET id_tache=:idTask WHERE id_utilisateur=:idUser AND id_projet=:idProjet");
        $this->deleteDevTask = $db->prepare("DELETE FROM TACHE_DEV WHERE id_utilisateur=:idUser AND id_projet=:idProjet");
    }

    public function insert($libelle, $projet) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':projet' => $projet));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectByIdProjet($projet) {
        $this->selectByIdProjet->execute(array(':projet' => $projet));
        if ($this->selectByIdProjet->errorCode() != 0) {
            print_r($this->selectByIdProjet->errorInfo());
        }
        return $this->selectByIdProjet->fetchAll();
    }

    public function update($id, $statut) {
        $r = true;
        $this->update->execute(array(':id' => $id, ':statut' => $statut));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectById($id) {
        $this->selectById->execute(array(':id' => $id));
        if ($this->selectById->errorCode() != 0) {
            print_r($this->selectById->errorInfo());
        }
        return $this->selectById->fetch();
    }

    public function delete($id) {
        $r = true;
        $this->delete->execute(array(':id' => $id));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectDevById($projet, $idDev) {
        $this->selectDevById->execute(array(':projet' => $projet, ':idUtilisateur' => $idDev));
        if ($this->selectDevById->errorCode() != 0) {
            print_r($this->selectDevById->errorInfo());
        }
        return $this->selectDevById->fetch();
    }

    public function sumCoutByProjet($projet) {
        $this->sumCoutByProjet->execute(array(':projet' => $projet));
        if ($this->sumCoutByProjet->errorCode() != 0) {
            print_r($this->sumCoutByProjet->errorInfo());
        }
        return $this->sumCoutByProjet->fetch();
    }

    public function insertTaskDev($idUser, $idProjet, $idTache) {
        $r = true;
        $this->insertTaskDev->execute(array(':id_utilisateur' => $idUser, ':id_projet' => $idProjet, ':id_tache' => $idTache));
        if ($this->insertTaskDev->errorCode() != 0) {
            print_r($this->insertTaskDev->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function updateTaskDev($idUser, $idProjet, $idTask) {
        $r = true;
        $this->updateTaskDev->execute(array(':idTask' => $idTask, ':idUser' => $idUser, ':idProjet' => $idProjet));
        if ($this->updateTaskDev->errorCode() != 0) {
            print_r($this->updateTaskDev->errorInfo());
            $r = false;
        }
        return $r;
    }
    public function deleteDevTask($idUser, $idProjet) {
        $r = true;
        $this->deleteDevTask->execute(array(':idUser' => $idUser, ':idProjet' => $idProjet));
        if ($this->deleteDevTask->errorCode() != 0) {
            print_r($this->deleteDevTask->errorInfo());
            $r = false;
        }
        return $r;
    }
    

}
?>



