<?php
class TachesDev{
    
    private $db;
    private $selectAll;
    private $selectAllByUser;
    private $selectAfaireByUser;
    private $selectEnCoursByUser;
    private $selectTermineeByUser;
    
    public function __construct($db){
        $this->db = $db;
        $this->selectAll = $db->prepare("select t.id, t.libelle as nomTache, p.nom as nomProjet, t.projet, t.statut from TACHES t INNER JOIN PROJET p ON t.projet=p.id");
        $this->selectAllByUser = $db->prepare("select t.*, p.* FROM TACHE_DEV td INNER JOIN TACHES t ON td.id_tache=t.id INNER JOIN PROJET p ON t.projet=p.id WHERE td.id_utilisateur=:id");
        $this->selectAfaireByUser = $db->prepare("select t.*, p.* FROM TACHE_DEV td INNER JOIN TACHES t ON td.id_tache=t.id INNER JOIN PROJET p ON t.projet=p.id WHERE td.id_utilisateur=:id AND t.statut=1");
        $this->selectEnCoursByUser = $db->prepare("select t.*, p.* FROM TACHE_DEV td INNER JOIN TACHES t ON td.id_tache=t.id INNER JOIN PROJET p ON t.projet=p.id WHERE td.id_utilisateur=:id AND t.statut=2");
        $this->selectTermineeByUser = $db->prepare("select t.*, p.* FROM TACHE_DEV td INNER JOIN TACHES t ON td.id_tache=t.id INNER JOIN PROJET p ON t.projet=p.id WHERE td.id_utilisateur=:id AND t.statut=3");
    }
    
    public function selectAll(){
        $this->selectAll->execute();
        if ($this->selectAll->errorCode()!=0){
             print_r($this->selectAll->errorInfo());  
        }
        return $this->selectAll->fetchAll();
    }
    
    public function selectAllByUser($id) {
        $this->selectAllByUser->execute(array(':id' => $id));
        if ($this->selectAllByUser->errorCode() != 0) {
            print_r($this->selectAllByUser->errorInfo());
        }
        return $this->selectAllByUser->fetchAll();
    }
    
    public function selectAfaireByUser($id) {
        $this->selectAfaireByUser->execute(array(':id' => $id));
        if ($this->selectAfaireByUser->errorCode() != 0) {
            print_r($this->selectAfaireByUser->errorInfo());
        }
        return $this->selectAfaireByUser->fetchAll();
    }
    
    public function selectEnCoursByUser($id) {
        $this->selectEnCoursByUser->execute(array(':id' => $id));
        if ($this->selectEnCoursByUser->errorCode() != 0) {
            print_r($this->selectEnCoursByUser->errorInfo());
        }
        return $this->selectEnCoursByUser->fetchAll();
    }
    
    public function selectTermineeByUser($id) {
        $this->selectTermineeByUser->execute(array(':id' => $id));
        if ($this->selectTermineeByUser->errorCode() != 0) {
            print_r($this->selectTermineeByUser->errorInfo());
        }
        return $this->selectTermineeByUser->fetchAll();
    }
}

?>

