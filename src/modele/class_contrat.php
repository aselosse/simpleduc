<?php

class Contrat{
    
    private $db;
    private $insert;
    private $selectAll;
    private $delete;
    private $update;
    private $selectContratById;
    private $selectLastByIdProjet;

    
    public function __construct($db){
        $this->db = $db;
        $this->insert = $db->prepare("insert into CONTRAT(id_projet, delai_de_production, date_signature, cout_global, echeance) values (:id_projet, :delai_de_production, :date_signature, :cout_global, :echeance)");    
        $this->selectAll = $db->prepare("select p.nom, c.delai_de_production, c.date_signature, c.cout_global, c.echeance from CONTRAT c inner join PROJET p ON c.id_projet = p.id");
        $this->delete = $db->prepare("delete from PROJET where id=:id");
        $this->update = $db->prepare("update CONTRAT set nom=:nom, chef=:chef,client=:client,description=:description where id=:id"); 
        $this->selectById = $db->prepare("select id,client, nom, chef,description from PROJET where id=:id order by nom");
        $this->selectLastByIdProjet = $db->prepare("SELECT * FROM CONTRAT c INNER JOIN PROJET j ON c.id_projet=j.id  WHERE j.id=:id GROUP BY id_projet");
        
    }
    
    public function insert($id_projet, $delai_de_production, $date_signature, $cout_global, $echeance){
        $r = true;        
        $this->insert->execute(array(':id_projet'=>$id_projet,':delai_de_production'=>$delai_de_production,':date_signature'=>$date_signature,':cout_global'=>$cout_global,':echeance'=>$echeance));
        if ($this->insert->errorCode()!=0){
             print_r($this->insert->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function selectAll(){
        $this->selectAll->execute();
        if ($this->selectAll->errorCode()!=0){
             print_r($this->selectAll->errorInfo());  
        }
        return $this->selectAll->fetchAll();
    }
    
    public function update($id, $nom, $chef, $client, $description){
        $r = true;        
        $this->update->execute(array(':id'=>$id,':nom'=>$nom,':chef'=>$chef,':client'=>$client,':description'=>$description));
        if ($this->update->errorCode()!=0){
             print_r($this->update->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function selectContratById($id){ 
        $this->selectContratById->execute(array(':id'=>$id)); 
        if ($this->selectContratById->errorCode()!=0){
            print_r($this->selectContratById->errorInfo()); 
            
        }
        return $this->selectContratById->fetch(); 
    }
    
    public function delete($id){
        $r = true;
        $this->delete->execute(array(':id'=>$id));
        if ($this->delete->errorCode()!=0){
             print_r($this->delete->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function selectLastByIdProjet($id){
        $this->selectLastByIdProjet->execute(array('id'=>$id));
        if ($this->selectLastByIdProjet->errorCode()!=0){
             print_r($this->seleselectLastByIdProjetct->errorInfo());  
        }
        return $this->selectLastByIdProjet->fetch();
    }    

}

?>



