<?php

class Client {

    private $db;
    private $insert;
    private $select;
    private $selectById;
    private $update;
    private $delete;

    public function __construct($db) {
        $this->db = $db;
        $this->insert        = $db->prepare("insert into CLIENT(nom, contact, coordonnee) values (:nom, :contact, :coordonnee)");
        $this->select        = $db->prepare("select id, nom, contact, coordonnee from CLIENT order by nom");
        $this->selectById    = $db->prepare("select id, nom, contact, coordonnee from CLIENT where id=:id");
        $this->update        = $db->prepare("update CLIENT set nom=:nom, contact=:contact, coordonnee=:coordonnee where id=:id");
        $this->delete        = $db->prepare("delete from CLIENT where id=:id");
    }

    public function insert($nom, $contact, $coordonnee) {
        $r = true;
        $this->insert->execute(array(':nom' => $nom, ':contact' => $contact, ':coordonnee' => $coordonnee));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectById($id) {
        $this->selectById->execute(array(':id' => $id));
        if ($this->selectById->errorCode() != 0) {
            print_r($this->selectById->errorInfo());
        }
        return $this->selectById->fetch();
    }

    public function update($id, $nom, $contact, $coordonnee) {
        $r = true;
        $this->update->execute(array(':id' => $id,':nom' => $nom, ':contact' => $contact, ':coordonnee' => $coordonnee));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($id) {
        $r = true;
        $this->delete->execute(array(':id' => $id));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

}
?>

