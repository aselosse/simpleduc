<?php

class Projet{
    
    private $db;
    private $insert;
    private $select;
    private $delete;
    private $update;
    private $selectById;
    private $selectByIdResponsable;
    private $selectDevById;
    private $selectNoDevById;

    
    public function __construct($db){
        $this->db = $db;
        $this->insert = $db->prepare("insert into PROJET(nom, chef, client, description) values (:nom, :chef, :client, :description)");    
        $this->select = $db->prepare("select p.id, u.nom, chef,description, p.nom as nomprojet, u.prenom, c.nom as nomCli from PROJET p inner join UTILISATEUR u on p.chef = u.id  inner join CLIENT c on p.client=c.id order by p.nom ");
        $this->delete = $db->prepare("delete from PROJET where id=:id");
        $this->update = $db->prepare("update PROJET set nom=:nom, chef=:chef,client=:client,description=:description where id=:id"); 
        $this->selectById = $db->prepare("select id,client, nom, chef,description from PROJET where id=:id order by nom");
        $this->selectByIdResponsable = $db->prepare("select id, nom, chef from PROJET where chef=:chef");
        $this->selectDevById = $db->prepare(" SELECT DISTINCT( u.id), u.nom, u.prenom "
                                          . " FROM UTILISATEUR u INNER JOIN TACHE_DEV td ON u.id = td.id_utilisateur"
                                                             . " INNER JOIN TACHES t ON td.id_tache = t.id"
                                                             . " INNER JOIN PROJET p ON t.projet = p.id"
                                          . " WHERE p.id = :idProjet");
        $this->selectNoDevById = $db->prepare(" SELECT id, nom, prenom "
                                          . " FROM UTILISATEUR "
                                          . " WHERE id NOT IN (SELECT u.id"
                                                            . " FROM UTILISATEUR u INNER JOIN TACHE_DEV td ON u.id = td.id_utilisateur"
                                                            . " INNER JOIN TACHES t ON td.id_tache = t.id"
                                                            . " INNER JOIN PROJET p ON t.projet = p.id"
                                                            . " WHERE p.id = :idProjet)");
    }
    
    public function insert($nom, $chef, $client, $description){
        $r = true;        
        $this->insert->execute(array(':nom'=>$nom,':chef'=>$chef,':client'=>$client,':description'=>$description));
        if ($this->insert->errorCode()!=0){
             print_r($this->insert->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function select(){
        $this->select->execute();
        if ($this->select->errorCode()!=0){
             print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
    public function selectByIdResponsable($chef){
        $this->selectByIdResponsable->execute(array(':chef'=>$chef));
        if ($this->selectByIdResponsable->errorCode()!=0){
             print_r($this->selectByIdResponsable->errorInfo());  
        }
        return $this->selectByIdResponsable->fetchAll();
    }
    
    public function update($id,$nom, $chef, $client, $description){
        $r = true;        
        $this->update->execute(array(':id'=>$id,':nom'=>$nom,':chef'=>$chef,':client'=>$client,':description'=>$description));
        if ($this->update->errorCode()!=0){
             print_r($this->update->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function selectById($id){ 
        $this->selectById->execute(array(':id'=>$id)); 
        if ($this->selectById->errorCode()!=0){
            print_r($this->selectById->errorInfo()); 
            
        }
        return $this->selectById->fetch(); 
    }
    
    public function delete($id){
        $r = true;
        $this->delete->execute(array(':id'=>$id));
        if ($this->delete->errorCode()!=0){
             print_r($this->delete->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function selectDevById($idProjet){ 
        $this->selectDevById->execute(array(':idProjet'=>$idProjet)); 
        if ($this->selectDevById->errorCode()!=0){
            print_r($this->selectDevById->errorInfo());            
        }
        return $this->selectDevById->fetchAll(); 
    }
    
    public function selectNoDevById($idProjet){ 
        $this->selectNoDevById->execute(array(':idProjet'=>$idProjet)); 
        if ($this->selectNoDevById->errorCode()!=0){
            print_r($this->selectNoDevById->errorInfo());            
        }
        return $this->selectNoDevById->fetchAll(); 
    }
    
    
}

?>



