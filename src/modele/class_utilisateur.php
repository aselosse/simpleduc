<?php

class Utilisateur {

    private $db;
    private $insert;
    private $connect;
    private $select;
    private $selectById;
    private $selectByRole;
    private $update;
    private $updateMdp;
    private $delete;

    public function __construct($db) {
        $this->db = $db;
        $this->insert        = $db->prepare("insert into UTILISATEUR(email, mdp, nom, prenom, role) values (:email, :mdp, :nom, :prenom, :role)");
        $this->connect       = $db->prepare("select id, email, mdp, role from UTILISATEUR where email=:email");
        $this->select        = $db->prepare("select u.id, email, role, nom, prenom, r.libelle as libellerole from UTILISATEUR u, ROLE r where u.role = r.id order by nom");
        $this->selectById    = $db->prepare("select email, nom, prenom, role from UTILISATEUR where id=:id");
        $this->selectByRole  = $db->prepare("select id,email, nom, prenom from UTILISATEUR where role=:role");
        $this->update        = $db->prepare("update UTILISATEUR set nom=:nom, prenom=:prenom, role=:role, email=:email where id=:id");
        $this->updateMdp     = $db->prepare("update UTILISATEUR set mdp=:mdp where email=:email");
        $this->delete        = $db->prepare("delete from UTILISATEUR where id=:id");
    }

    public function insert($email, $mdp, $role, $nom, $prenom) {
        $r = true;
        $this->insert->execute(array(':email' => $email, ':mdp' => $mdp, ':role' => $role, ':nom' => $nom, ':prenom' => $prenom));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function connect($email) {
        $unUtilisateur = $this->connect->execute(array(':email' => $email));
        if ($this->connect->errorCode() != 0) {
            print_r($this->connect->errorInfo());
        }
        return $this->connect->fetch();
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectById($id) {
        $this->selectById->execute(array(':id' => $id));
        if ($this->selectById->errorCode() != 0) {
            print_r($this->selectById->errorInfo());
        }
        return $this->selectById->fetch();
    }
    public function selectByRole($role) {
        $this->selectByRole->execute(array(':role' => $role));
        if ($this->selectByRole->errorCode() != 0) {
            print_r($this->selectByRole->errorInfo());
        }
        return $this->selectByRole->fetchAll();
    }

    public function update($id, $email, $role, $nom, $prenom) {
        $r = true;
        $this->update->execute(array(':id' => $id,':email' => $email, ':role' => $role, ':nom' => $nom, ':prenom' => $prenom));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function updateMdp($email, $mdp) {
        $r = true;
        $this->updateMdp->execute(array(':email' => $email, ':mdp' => $mdp));
        if ($this->update->errorCode() != 0) {
            print_r($this->updateMdp->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($id) {
        $r = true;
        $this->delete->execute(array(':id' => $id));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

}
?>

