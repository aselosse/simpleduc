<?php

class Competences {

    private $db;
    private $selectAll;
    private $selectByUser;
    private $insert;
    private $delete;
    private $deleteByComp;

    public function __construct($db) {
        $this->db = $db;
        $this->selectAll = $db->prepare("select id, libelle, version, cout FROM COMPETENCE");
        $this->selectByUser = $db->prepare("select c.* FROM COMPETENCE_UTILISATEUR cu INNER JOIN COMPETENCE c ON cu.id_competence=c.id WHERE cu.id_utilisateur=:id");
        $this->insert = $db->prepare("insert into COMPETENCE(libelle, version, cout) values (:libelle, :version, :cout)");
        $this->delete = $db->prepare("delete from COMPETENCE where id=:id");
        $this->deleteByComp = $db->prepare("delete from COMPETENCE_UTILISATEUR where id_competence=:id");
    }

    public function selectAll() {
        $this->selectAll->execute();
        if ($this->selectAll->errorCode() != 0) {
            print_r($this->selectAll->errorInfo());
        }
        return $this->selectAll->fetchAll();
    }

    public function selectByUser($id) {
        $this->selectByUser->execute(array(':id' => $id));
        if ($this->selectByUser->errorCode() != 0) {
            print_r($this->selectByUser->errorInfo());
        }
        return $this->selectByUser->fetchAll();
    }
    
    public function insert($libelle, $version, $cout) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':version' => $version, ':cout' => $cout));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }
    
    public function delete($id) {
        $r = true;
        $this->delete->execute(array(':id' => $id));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }
    
    public function deleteByComp($id) {
        $r = true;
        $this->deleteByComp->execute(array(':id' => $id));
        if ($this->deleteByComp->errorCode() != 0) {
            print_r($this->deleteByComp->errorInfo());
            $r = false;
        }
        return $r;
    }
}
?>
