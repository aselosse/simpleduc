<?php

function actionCompteModif($twig, $db) {
    $form = array();
    if (isset($_SESSION['id'])) {
        $utilisateur = new Utilisateur($db);
        if (isset($_POST['btEnregistrer'])) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $email = $_POST['email'];
            $exec = $utilisateur->update($_SESSION['id'], $email, $_SESSION['role'], $nom, $prenom);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Echec de la modification des données. ';
            } else {
                $form['valide'] = true;
                $form['message'] = 'Modification des données réussie. ';
            }
            if (!empty($_POST['inputPassword'])) {
                $p1 = $_POST['inputPassword'];
                $p2 = $_POST['inputPassword2'];
                if ($p1 == $p2) {
                    $p1 = password_hash($p1, PASSWORD_DEFAULT);
                    $exec = $utilisateur->updateMdp($email, $p1);
                    if (!$exec) {
                        $form['valide'] = false;
                        $form['message'] .= 'Echec de la modification du mot de passe';
                    } else {
                        $form['valide'] = true;
                        $form['message'] .= 'Modification réussie du mot de passe';
                    }
                } else {
                    $form['valide'] = false;
                    $form['message'] .= 'Echec de la modification du mot de passe';
                }
            }
        }
        
        $unUtilisateur = $utilisateur->selectById($_SESSION['id']);
        if ($unUtilisateur != null) {
            $form['utilisateur'] = $unUtilisateur;
        } else {
            $form['message'] = 'Utilisateur incorrect';
        }
    }
    echo $twig->render('compte-modif.html.twig', array('form' => $form));
}
