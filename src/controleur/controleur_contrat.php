<?php

function actionContrat($twig, $db) {
    $form = array();
    $contrat = new Contrat($db);
    $selectAll = $contrat->selectAll();
    echo $twig->render('contrat.html.twig', array('form' => $form, 'selectAll' => $selectAll));
}

function actionContratAjout($twig, $db){
    $form = array(); 
    $projet = new Projet($db);
    if (isset($_POST['btAjouter'])){
      $inputIdProjet = $_POST['inputIdProjet'];
      $inputDelaiDeProduction = $_POST['inputDelaiDeProduction'];   
      $inputDateSignature = $_POST['inputDateSignature'];
      $inputCoutGlobal = $_POST['inputCoutGlobal'];
      $inputEcheance = $_POST['inputEcheance'];
      $form['valide'] = true;
      $contrat = new Contrat($db); 
      $exec = $contrat->insert($inputIdProjet, $inputDelaiDeProduction, $inputDateSignature, $inputCoutGlobal, $inputEcheance);
      if (!$exec){
        $form['valide'] = false;  
        $form['message'] = 'Problème d\'insertion dans la table contrat ';  
      }
    }
    $lesProjets = $projet->select();
    echo $twig->render('contrat-ajout.html.twig', array('form'=>$form, 'projets'=>$lesProjets)); 
}

function actionContratModif($twig, $db) {
    $form = array();
    if (isset($_GET['id'])) {
        $contrat = new Contrat($db);
        $unContrat = $contrat->selectContratById($_GET['id']);
        if ($unContrat != null) {
            $form['contrat'] = $unContrat;            
        } else {
            $form['message'] = 'Contrat incorrect';
        }
    } else {
        if (isset($_POST['btModifier'])) {
            $contrat = new Contrat($db);
            $idProjet = $_POST['idProjet'];
            $delaiDeProduction = $_POST['delaiDeProduction'];
            $dateSignature = $_POST['dateSignature'];
            $coutGlobal = $_POST['coutGlobal'];
            $echeance = $_POST['echeance'];
            $exec = $contrat->update($idProjet, $delaiDeProduction, $dateSignature, $coutGlobal, $echeance);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Echec de la modification des données. ';
            } else {
                $form['valide'] = true;
                $form['message'] = 'Modification des données réussie. ';
            }            
        } else {
            $form['message'] = 'Contrat non précisé';
        }
    }
    echo $twig->render('contrat-modif.html.twig', array('form' => $form));
}