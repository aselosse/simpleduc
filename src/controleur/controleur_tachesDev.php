<?php

function actionTachesDev($twig, $db) {
    $form = array();
    $taches = new TachesDev($db);
    $listeAll = $taches->selectAll();
    $toutesVosTaches = $taches->selectAllByUser($_SESSION['id']);
    $toutesVosTachesAfaire = $taches->selectAfaireByUser($_SESSION['id']);
    $toutesVosTachesEnCours = $taches->selectEnCoursByUser($_SESSION['id']);
    $toutesVosTachesTerminee = $taches->selectTermineeByUser($_SESSION['id']);
    echo $twig->render('tachesDev.html.twig', array('form' => $form, 'listeAll' => $listeAll, 
                        'toutesVosTaches' => $toutesVosTaches, 'toutesVosTachesAfaire' => $toutesVosTachesAfaire, 
                        'toutesVosTachesEnCours' => $toutesVosTachesEnCours, 'toutesVosTachesTerminee' => $toutesVosTachesTerminee));
}