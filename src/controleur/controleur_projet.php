<?php

function actionProjet($twig, $db) {
    $form = array();
    $projet = new Projet($db);

    if (isset($_GET['id'])) {
        $exec = $projet->delete($_GET['id']);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème de suppression dans la table projet';
        } else {
            $form['valide'] = true;
        }
        $form['message'] = 'Projet supprimée avec succès';
    }

    $liste = $projet->select();
    echo $twig->render('projet.html.twig', array('form' => $form, 'liste' => $liste));
}

function actionProjetAjout($twig, $db) {
    $form = array();
    if (isset($_POST['btAjouter'])) {
        $nom = $_POST['inputNom'];
        $chef = $_POST['inputIdResponsable'];
        $client = $_POST['inputIdClient'];
        $description = $_POST['inputDescription'];
        $form['valide'] = true;
        $projet = new Projet($db);
        $exec = $projet->insert($nom, $chef, $client, $description);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table projet ';
        }
    } 
    else {
        $utilisateur = new Utilisateur($db);
        $liste = $utilisateur->selectByRole(1);
        $form['liste'] = $liste;

        $client = new Client($db);
        $listeClient = $client->select();
        $form['listeClient'] = $listeClient;
    }
    echo $twig->render('projet-ajout.html.twig', array('form' => $form));
}

function actionProjetModif($twig, $db) {
    $form = array();
    if (isset($_GET['id'])) {
        $projet = new Projet($db);
        $unProjet = $projet->selectById($_GET['id']);

        if ($unProjet != null) {
            $form['projet'] = $unProjet;
            
            $client = new Client($db);
            $listeclient = $client->select();
            $form['listeClient'] = $listeclient;
            
            $utilisateur = new Utilisateur($db);
            $liste = $utilisateur->select();
            $form['liste'] = $liste;
        } else {
            $form['message'] = 'Projet incorrecte';
        }
    } else {
        if (isset($_POST['btModifier'])) {
            $id = $_POST['id'];
            $nom = $_POST['inputNom'];
            $chef = $_POST['inputIdResponsable'];
            $client = $_POST['inputIdClient'];       
            $description = $_POST['inputDescription'];
            $projet = new Projet($db);           
            $exec = $projet->update($id,$nom, $chef, $client, $description);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] .= 'Echec de la modification del\'équipe';
            } else {
                $form['valide'] = true;
                $form['message'] = 'Modification réussie';
            }
        } else {
            $form['message'] = 'Utilisateur non précisé';
        }
    }

    echo $twig->render('projet-modif.html.twig', array('form' => $form));
}

// WebService
function actionProjetWS($twig, $db) {
    $projet = new Projet($db);
    $json = json_encode($liste = $projet->select());
    echo $json;
}
