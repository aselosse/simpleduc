<?php

function actionTache($twig, $db) {
    $form = array();
    $liste = array();
    $tache = new Tache($db);
    $projet = new Projet($db);
    $contrat = new Contrat($db);
    if (isset($_GET['id'])) {
        $tachelist = $tache->selectByIdProjet($_GET['id']);
        $liste['afaire'] = array();
        $liste['enCours'] = array();
        $liste['terminee'] = array();
        foreach ($tachelist as $t) {
            $statut = $t['statut'];
            if ($statut == 1) {
                array_push($liste['afaire'], $t);
            }
            if ($statut == 2) {
                array_push($liste['enCours'], $t);
            }
            if ($statut == 3) {
                array_push($liste['terminee'], $t);
            }
        }
        $listprojet = $projet->selectById($_GET['id']);
        $liste['idProjet'] = $_GET['id'];
        $liste['nomProjet'] = $listprojet['nom'];
        $lastContrat=$contrat->selectLastByIdProjet($_GET['id']);
        $liste['coutProjet'] =$lastContrat['cout_global'];
        $sumCout = $tache->sumCoutByProjet($_GET['id']);
        $liste['coutReel'] = $sumCout['cout'];        
    }
//    echo'<pre>';    print_r($lastContrat);
//    echo'</pre>';
    echo $twig->render('taches.html.twig', array('form' => $form,'liste' => $liste, 'tachelist' => $tachelist));
}

function actionTacheAjout($twig, $db) {

    $form = array();
    $liste = array();
    if (isset($_POST['btAjouter'])) {
        $libelle = $_POST['inputLibelle'];
        $projet = $_POST['idProjet'];
        $form['valide'] = true;
        $tache = new Tache($db);
        $exec = $tache->insert($libelle, $projet);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table tache ';
        }
        $liste['idProjet'] = $_POST['idProjet'];
    } else {
        $liste['idProjet'] = $_GET['id'];
    }
    echo $twig->render('tache-ajout.html.twig', array('form' => $form, 'liste' => $liste));
}
