<?php
 

function actionDeveloppeur($twig, $db) {
    
    $projet = new Projet($db);
    $listeProjet = $projet->select();
    for($i=0;$i<count($listeProjet);$i++){
        $listeProjet[$i]['listeDev'] = $projet->selectDevById($listeProjet[$i]['id']);    
    }
//    echo '<pre>';
//    print_r($listeProjet);
    
    echo $twig->render('developpeur.html.twig', array('listeProjet' => $listeProjet));
}

function actionDeveloppeurAffectation($twig, $db) {
    $form = array();
    
    $projet = new Projet($db);
    $tache = new Tache($db);
    $listeUtilisateur = $projet->selectNoDevById($_GET['idProjet']);
    $listeUtilisateurProjet = $projet->selectDevById($_GET['idProjet']);
    
    for($i=0;$i<count($listeUtilisateurProjet);$i++){
        $listeUtilisateurProjet[$i]['taches'] = $tache->selectDevById($_GET['idProjet'], $listeUtilisateurProjet[$i]['id']);
    }
    $listeTache = $tache->selectByIdProjet($_GET['idProjet']);
    
    
//    echo '<pre>';
//    print_r($listeUtilisateurProjet);
//    echo '</pre>';
    echo $twig->render('developpeur-affectation.html.twig', array('form' => $form, 'listeTache' => $listeTache, 'listeUtilisateur' => $listeUtilisateur, 'listeUtilisateurProjet' => $listeUtilisateurProjet));
}