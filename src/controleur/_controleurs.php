<?php
  require_once 'controleur_index.php';
  require_once 'controleur_utilisateur.php';
  require_once 'controleur_projet.php';
  require_once 'controleur_client.php';
  require_once 'controleur_competences.php';
  require_once 'controleur_tache.php';
  require_once 'controleur_compte.php';
  require_once 'controleur_ajax.php';
  require_once 'controleur_developpeur.php';
  require_once 'controleur_tachesDev.php';
  require_once 'controleur_contrat.php';
?>

