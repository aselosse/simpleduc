<?php

function actionClient($twig, $db) {
    $form = array();
    $client = new Client($db);
    $contrat = new Contrat($db);    
    if (isset($_GET['id'])) {    
        $exec = $client->delete($_GET['id']);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème de suppression dans la table client';
        } else {
            $form['valide'] = true;
            $form['message'] = 'Client supprimé avec succès';
        }
    }
    $listeClient = $client->select();    
    for($i=0;$i<count($listeClient);$i++){
        $listeClient[$i]['projets'] = $contrat->selectLastByIdProjet(listeClient[$i]['id']);
    }    
    echo $twig->render('client.html.twig', array('form' => $form, 'listeClient' => $listeClient));
}

function actionClientModif($twig, $db) {
    $form = array();
    if (isset($_GET['id'])) {
        $client = new Client($db);
        $unClient = $client->selectById($_GET['id']);
        if ($unClient != null) {
            $form['client'] = $unClient;            
        } else {
            $form['message'] = 'Client incorrect';
        }
    } else {
        if (isset($_POST['btModifier'])) {
            $client = new Client($db);
            $id = $_POST['id'];
            $nom = $_POST['nom'];
            $contact = $_POST['contact'];
            $coordonnee = $_POST['coordonnee'];
            $exec = $client->update($id, $nom, $contact, $coordonnee);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Echec de la modification des données. ';
            } else {
                $form['valide'] = true;
                $form['message'] = 'Modification des données réussie. ';
            }            
        } else {
            $form['message'] = 'Client non précisé';
        }
    }
    echo $twig->render('client-modif.html.twig', array('form' => $form));
}

function actionClientAjout($twig, $db){
    $form = array(); 
    if (isset($_POST['btAjouter'])){
      $inputNom = $_POST['inputNom'];
      $inputContact = $_POST['inputContact'];   
      $inputCoordonnee = $_POST['inputCoordonnee'];      
      $form['valide'] = true;
      $client = new Client($db); 
      $exec = $client->insert($inputNom, $inputContact, $inputCoordonnee);
      if (!$exec){
        $form['valide'] = false;  
        $form['message'] = 'Problème d\'insertion dans la table équipe ';  
      }
    }
    else{        
        $client = new Client($db);
        $listeClient = $client->select();
        $form['listeClient'] = $listeClient;
    } 
    echo $twig->render('client-ajout.html.twig', array('form'=>$form)); 
}
?>

