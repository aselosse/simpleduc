<?php

function actionAjax($twig, $db) {
    if ($_GET['type'] == "task") {
        $task = new Tache($db);
        if ($task->update($_POST['id'], $_POST['statut'])) {
            echo 'ok';
        }
    }

    if ($_GET['type'] == "dev_task") {

        $utilisateur = new Utilisateur($db);
        $tache = new Tache($db);

        $unUtilisateur = $utilisateur->selectById($_GET['idDev']);
        $unUtilisateur['taches'] = $tache->selectDevById($_GET['idProjet'], $_GET['idDev']);
        $listeTache = $tache->selectByIdProjet($_GET['idProjet']);
        ?>
        <tr id="devAffect_<?php echo $_GET['idDev']; ?>">
            <td><?php echo $unUtilisateur['nom'] . ' ' . $unUtilisateur['prenom']; ?></td>  
            <td>
                <select name="tacheSelect" onchange="addTask(<?php echo $_GET['idDev']; ?>, this.value);">
                    <option value="0" selected disabled> Aucune tâche </option>
                    <?php
                    for ($t = 0; $t < count($listeTache); $t++) {
                        ?><option value="<?php echo $listeTache[$t]['id'] ?>"><?php echo $listeTache[$t]['libelle'] ?></option><?php
                    }
                    ?>
                </select>
            </td> 
        </tr>
        <?php
    }


    if ($_GET['type'] == "add_task") {

        $tache = new Tache($db);
        $dev = $tache->selectDevById($_POST['projet'], $_POST['dev']);
        if (empty($dev)) {
            $exec = $tache->insertTaskDev($_POST['dev'], $_POST['projet'], $_POST['task']);
        }
        else{
            $exec = $tache->updateTaskDev($_POST['dev'], $_POST['projet'], $_POST['task']);
            
        }
        
    }

    if ($_GET['type'] == "del_task") {
        $tache = new Tache($db);
        $exec = $tache->deleteDevTask($_POST['dev'], $_POST['projet']);
        
    }
}
