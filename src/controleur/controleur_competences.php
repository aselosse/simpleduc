<?php

function actionCompetences($twig, $db) {
    $form = array();
    $competences = new Competences($db);
    $liste = $competences->selectAll();
    $voscompetences = $competences->selectByUser($_SESSION['id']);
    $list_competence = array();
    foreach ($voscompetences as $i => $c) {
        array_push($list_competence, $c['id']);
    }
    
    if(isset($_POST['btSupprimer'])){
        $cocher = $_POST['cocher'];
        $form['valide'] = true;
        foreach ($cocher as $id){
          $competences->deleteByComp($id);  
          $exec=$competences->delete($id); 
          if (!$exec){
             $form['valide'] = false;  
             $form['message'] = 'Problème de suppression dans la table competences';   
          }
        }
    }
    
    if (isset($_GET['id'])) {    
        $exec = $competences->delete($_GET['id']);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème de suppression dans la table COMPETENCE';
        } else {
            $form['valide'] = true;
            $form['message'] = 'Compétence supprimé avec succès';
        }
    }
    if (isset($_POST['btAjouter'])) {
        $inputLibelle = $_POST['inputLibelle'];
        $inputVersion = $_POST['inputVersion'];
        $inputCout = $_POST['inputCout'];
        $form['valide'] = true;
        $competences = new Competences($db);
        $exec = $competences->insert($inputLibelle, $inputVersion, $inputCout);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table COMPETENCE ';
        }
    }
    
    $liste = $competences->selectAll();
    echo $twig->render('competences.html.twig', array('form' => $form, 'liste' => $liste, 'voscompetences' => $voscompetences, 'list_competence' => $list_competence));
    }
