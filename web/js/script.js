function task(dir, id, statut, arrow) {
    if (dir == "left" && statut > 1 || dir == "right" && statut < 3) {
        var div = document.getElementById("task_" + id);
        if (statut == 1) {
            new_statut = 2;
            document.getElementById("cours").appendChild(div);
        }
        if (statut == 3) {
            new_statut = 2;
            document.getElementById("cours").appendChild(div);
        }
        if (statut == 2) {
            if (dir == "left") {
                new_statut = 1;
                document.getElementById("faire").appendChild(div);
            } else {
                new_statut = 3;
                document.getElementById("minee").appendChild(div);
            }
        }
        arrow.removeAttribute("onclick");
        arrow.setAttribute("onclick", "task('" + dir + "', " + id + "," + new_statut + ", this)");
        if (dir == "left") {
            document.getElementById("right_" + id).removeAttribute("onclick");
            document.getElementById("right_" + id).setAttribute("onclick", "task('right'," + id + "," + new_statut + ", this)");
        } else {
            document.getElementById("left_" + id).removeAttribute("onclick");
            document.getElementById("left_" + id).setAttribute("onclick", "task('left'," + id + "," + new_statut + ", this)");
        }

        sendNewStatut(id, new_statut);
    }
}

function sendNewStatut(id, statut) {
    $.post("index.php?page=ajax&type=task", {id: id, statut: statut}, function (data) {

    });
}

function changeAffect(devId) {
    var dev = document.getElementById("dev_" + devId);

    console.log(dev);
    if (dev.parentNode.id == "noAffect") {
        var newDev = createA(dev, devId);
        newDev.appendChild(arrow('left', devId));
        newDev.innerHTML += "<span id='text_" + devId + "'>" + document.getElementById("text_" + devId).innerHTML + "</span>";
        document.getElementById("affect").appendChild(newDev);
        reloadTable(document.getElementById("idProjet").value, devId);

    }
    if (dev.parentNode.id == "affect") {
        var newDev = createA(dev, devId);
        newDev.innerHTML += "<span id='text_" + devId + "'>" + document.getElementById("text_" + devId).innerHTML + "</span>";
        newDev.appendChild(arrow('right', devId));
        document.getElementById("noAffect").appendChild(newDev);
        document.getElementById("devAffect_" + devId).remove();
        delTask(devId);
    }
    dev.remove();
}

function createA(content, idDev) {
    console.log(content.onmouseover)
    var newElmnt = document.createElement('li');
    newElmnt.setAttribute("id", 'dev_' + idDev);
    newElmnt.setAttribute("onclick", "changeAffect('" + idDev + "')");
    newElmnt.setAttribute("onmouseover", "document.getElementById('arrow_" + idDev + "').style.visibility = 'visible';");
    newElmnt.setAttribute("onmouseout", "document.getElementById('arrow_" + idDev + "').style.visibility = 'hidden';");
    return newElmnt;
}

function arrow(sens, id) {
    var retour = document.createElement('span');
    retour.setAttribute('id', 'arrow_' + id);
    retour.setAttribute('style', 'visibility: hidden;');
    retour.setAttribute('class', ' glyphicon glyphicon-arrow-' + sens);
    return retour;
}

function reloadTable(idPro, devId) {
    $.post("index.php?page=ajax&type=dev_task&idProjet=" + idPro + "&idDev=" + devId, {}, function (data) {
        document.getElementById("developpeur_task").innerHTML += data;
    });
}

function addTask(idDev, idTask) {


    $.post("index.php?page=ajax&type=add_task", {dev: idDev, task: idTask, projet: document.getElementById("idProjet").value}, function (data) { });
}

function delTask(idDev) {


    $.post("index.php?page=ajax&type=del_task", {dev: idDev, projet: document.getElementById("idProjet").value}, function (data) { });
}

function deleteCheck() {

        if (document.getElementById("checkAll").checked === true) {
            document.getElementsByName("cocher[]").forEach(function (caase) {
                caase.checked = true;
            });
        } else {
            document.getElementsByName("cocher[]").forEach(function (caase) {
                caase.checked = false;
            });
        }
}
